// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerChar.h"
#include "GameFramework/Character.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/CharacterMovementComponent.h"



// Sets default values
APlayerChar::APlayerChar()
{
	PrimaryActorTick.bCanEverTick = true;

	cam = CreateDefaultSubobject<UCameraComponent>(TEXT("MainCam"));
	cam->SetupAttachment(GetCapsuleComponent());
	cam->bUsePawnControlRotation = true;

	hingeMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("HingeWeapon"));
	hingeMesh->SetupAttachment(cam);
	hingeMesh->bCastDynamicShadow = true;
	hingeMesh->CastShadow = false;
	// hingeMesh->SetRelativeLocation(FVector())
	hingeMesh->SetOnlyOwnerSee(true);

	UCharacterMovementComponent* cmove = GetCharacterMovement();
	cmove->BrakingFriction = 10.f;
	cmove->MaxAcceleration = 10000.f;
	cmove->MaxWalkSpeed = 1000.f;
	cmove->JumpZVelocity = 570.f;
	cmove->AirControl = 2.f;
}

// Called when the game starts or when spawned
void APlayerChar::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void APlayerChar::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (hp <= 0) {
		UE_LOG(LogTemp, Warning, TEXT("Has Morido"));
		UGameplayStatics::OpenLevel(GetWorld(), "Muelto");
	}
}

// Called to bind functionality to input
void APlayerChar::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("Shoot", IE_Pressed, this, &APlayerChar::OnUseItem);

	PlayerInputComponent->BindAction("CambioArma", IE_Pressed, this, &APlayerChar::OnChange);

	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &APlayerChar::Run);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &APlayerChar::NotRun);
	
	PlayerInputComponent->BindAxis("Forward", this, &APlayerChar::MoveForward);
	PlayerInputComponent->BindAxis("Right", this, &APlayerChar::MoveRight);

	PlayerInputComponent->BindAxis("MouseRight", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("MouseUp", this, &APawn::AddControllerPitchInput);

}

void APlayerChar::MoveForward(float vel) {
	if (vel != 0) {
		UCharacterMovementComponent* cmove = GetCharacterMovement();
		if (Running == false) {
			AddMovementInput(GetActorForwardVector(), vel * moveSpeed);
			cmove->MaxWalkSpeed = 1000.f;
			//UE_LOG(LogTemp, Warning, TEXT("adelante"));
		}
		else {
			AddMovementInput(GetActorForwardVector(), vel * runSpeed);
			cmove->MaxWalkSpeed = 2000.f;
			//UE_LOG(LogTemp, Warning, TEXT("adelanterapido"));
		}
		
	}
}

void APlayerChar::MoveRight(float vel) {
	if (vel != 0) {
		UCharacterMovementComponent* cmove = GetCharacterMovement();
		if (Running == false) {
			AddMovementInput(GetActorRightVector(), vel * moveSpeed);
			cmove->MaxWalkSpeed = 1000.f;
			//UE_LOG(LogTemp, Warning, TEXT("andelao"));
		}
		else {
			AddMovementInput(GetActorRightVector(), vel * runSpeed);
			cmove->MaxWalkSpeed = 2000.f;
			//UE_LOG(LogTemp, Warning, TEXT("andelaorapido"));
		}
	}
}

void APlayerChar::Run() {
	Running = true;
}

void APlayerChar::NotRun() {
	Running = false;
}

void APlayerChar::OnUseItem(){
	evOnUseItem.Broadcast();
}

void APlayerChar::OnChange() {
	if (NWeapon > 1)
	evOnChange.Broadcast();
}

void APlayerChar::Hit(float dmg) {
	hp = hp - dmg;
}
