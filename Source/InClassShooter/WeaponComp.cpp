// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponComp.h"
#include "Enemychar.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values for this component's properties
UWeaponComp::UWeaponComp()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UWeaponComp::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UWeaponComp::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UWeaponComp::AttachToPlayer(APlayerChar* target){

	selfActor = target;
	if(selfActor){

		FAttachmentTransformRules rules(EAttachmentRule::SnapToTarget, true);
		GetOwner()->AttachToComponent(selfActor->GetWeaponPoint(), rules, TEXT("WeaponPoint"));
		
		selfActor->evOnUseItem.AddDynamic(this, &UWeaponComp::Fire);
		selfActor->evOnChange.AddDynamic(this, &UWeaponComp::Cambio);
		Equipado = true;
		if (selfActor->HasWeapon == true) {
			Equipado = false;
			GetOwner()->SetActorHiddenInGame(true);
		}
		selfActor->HasWeapon = true;
		selfActor->NWeapon += 1;
	}

	
}

void UWeaponComp::Cambio() {
	if (Equipado) {
		Equipado = false;
		GetOwner()->SetActorHiddenInGame(true);
	}
	else {
		Equipado = true;
		GetOwner()->SetActorHiddenInGame(false);
	}
}

void UWeaponComp::Fire(){
	if(!selfActor || !selfActor->GetController())
		return;
	if (Equipado == false)
		return;

	UE_LOG(LogTemp, Warning, TEXT("pium"));

	UWorld* const w = GetWorld();
	UCameraComponent* cam = selfActor->GetCam();
	FVector const vStart = cam->GetComponentLocation();
	FVector const vEnd = vStart + UKismetMathLibrary::GetForwardVector(cam->GetComponentRotation()) * 5000;

	FCollisionQueryParams inParams;
	inParams.AddIgnoredActor(selfActor);

	FCollisionResponseParams outParams;	
	FHitResult hit;
	if (armaTipo == 0) {
		w->LineTraceSingleByChannel(hit, vStart, vEnd, ECC_Camera, inParams, outParams);
		DrawDebugLine(w, vStart, vEnd, hit.bBlockingHit ? FColor::Red : FColor::Blue, false,
			5.f, 0, 10.f);
		UE_LOG(LogTemp, Warning, TEXT("Trace %s to %s"), *vStart.ToCompactString(), *vEnd.ToCompactString());

		if (hit.bBlockingHit && IsValid(hit.GetActor())) {
			UE_LOG(LogTemp, Warning, TEXT("Golpié %s"), *(hit.GetActor()->GetName()));
			
			AEnemyChar* e = Cast<AEnemyChar>(hit.GetActor());
			if (e) {
				e->BeDamaged(damage);
				// e->Hit(x);
			}


		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("No >:("));
		}

	}
	else {
		for (size_t i = 0; i < 14; i++)
		{
			FVector const vEnder = vStart + UKismetMathLibrary::GetForwardVector(cam->GetComponentRotation()) * 2000 + FVector(FMath::RandRange(-200, 200), FMath::RandRange(-200, 200), FMath::RandRange(-200, 200));
			w->LineTraceSingleByChannel(hit, vStart, vEnder, ECC_Camera, inParams, outParams);
			DrawDebugLine(w, vStart, vEnder, hit.bBlockingHit ? FColor::Red : FColor::Blue, false,
				5.f, 0, 10.f);
			UE_LOG(LogTemp, Warning, TEXT("Trace %s to %s"), *vStart.ToCompactString(), *vEnder.ToCompactString());

			if (hit.bBlockingHit && IsValid(hit.GetActor())) {
				UE_LOG(LogTemp, Warning, TEXT("Golpié %s"), *(hit.GetActor()->GetName()));

				AEnemyChar* e = Cast<AEnemyChar>(hit.GetActor());
				if (e) {
					e->BeDamaged(damage);
				}

			}
		}
	}
}









