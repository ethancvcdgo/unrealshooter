# InClassShooter

Developed with Unreal Engine 5


- Risketos Bàsics
1. Control de personatge amb base ACharacter
2. Capacitat de disparar amb Trace line
3. Ha d’implementar una petita inteligencia artificial als enemics
4. Usar delegados para alguna acción, ejemplo disparar
5. Més d’un tipus d'armes i/o bales (Tienes pistola y escopeta con dispersión de balas)
6. Ús de les Macros d’Unreal Engine per privatització, escritura i lectura 


- Risketos Opcionals
1. Control de jugador avançat (Pots esprintar i caminar normal)
2. Shaders (enemigo to cutre)
3. Puedes recoger armas y cambiar de arma entre las dos opciones que hay con F
4. Puedes fenecer si un enemigo te pega a melee y te envia a otro nivel vacio
